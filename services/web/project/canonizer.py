import os
import csv
from lemmagen3 import Lemmatizer


BASEDIR = os.path.dirname(__file__)

def lem_adj(gender, wrd, lem_genders):
    lem = lem_genders[gender]
    form = lem.lemmatize(wrd)
    return form


def process_nlp_pipeline(text, nlp):
    doc = nlp(text)
    return doc


def get_adj_msd(head, word):
    feats = head.feats
    feats_dict = {}
    feats = feats.strip().split('|')
    for f in feats:
        f = f.strip().split('=')
        feats_dict[f[0]] = f[1]
    gender = feats_dict['Gender']
    #print(gender)
    #gender = gender.strip().split('=')[1]
    if gender == 'Masc' and len(word.xpos) == 6:
        msd = word.xpos[:-1]+'ny'
    elif gender == 'Masc' and len(word.xpos) == 7:
        msd = word.xpos[:-1]+'y'
    elif gender == 'Fem':
        msd = word.xpos[:-1]+'n'
    elif gender == 'Neut':
        msd = word.xpos[:-1]+'n'
    else:
        msd = None
    return msd


def subfinder(mylist, pattern):
    matches = []
    for i in range(len(mylist)):
        if mylist[i].text.lower() == pattern[0] and [t.text.lower() for t in mylist[i:i+len(pattern)]] == pattern:
            matches.append(mylist[i:i+len(pattern)])
    return matches


def find_canon(term, lem_genders):
    head = None
    pre = []
    post = []
    for word in term.words:
        if word.upos == 'NOUN' or word.upos == 'PROPN':
            head = word
            break
    if head is None:
        if len(term.words) == 1:
            head2 = term.words[0]
            lem = Lemmatizer()
            lem.load_model(os.path.join(BASEDIR, 'lemmagen_models/kanon.bin'))
            head_form = lem.lemmatize(head2.text.lower())
            return head_form
        else:
            return 'HEAD not found'
    else:
        for word in term.words:
            if word.id < head.id:
                pre.append(word)
            elif word.id > head.id:
                post.append(word)

    canon = []
    for el in pre:
        msd = get_adj_msd(head, el)
        if msd is None:
            canon.append(el.lemma.lower())
        else:
            if msd[0] == 'A' and msd[3] in lem_genders:
                gender = msd[3]
                lem = lem_genders[gender]
                wrd =  el.text.lower()
                form = lem.lemmatize(wrd)
                canon.append(form)

    lem = lem_genders['base']
    head_form = lem.lemmatize(head.text.lower())
    canon.append(head_form)
    for el in post:
        canon.append(el.text)
    return ' '.join(canon)

def process(forms, nlp, lem_genders):
    text = '\n'.join(forms)
    doc = process_nlp_pipeline(text, nlp)
    return [find_canon(sent, lem_genders) for sent in doc.sentences]


def read_csv(fname, columnID=0):
    data = []
    with open(fname) as csvfile:
        try:
            dialect = csv.Sniffer().sniff(csvfile.read(2048))
        except csv.Error:
            print('Warning: cannot determine delimiter, assuming Excel CSV dialect.')
            dialect = 'excel'
        csvfile.seek(0)
        reader = csv.reader(csvfile, dialect)
        for i, row in enumerate(reader):
            try:
                data.append(row[columnID])
            except:
                print('Error, line {}'.format(i))
    return data
