import pandas as pd
from .preprocessing import batchify_docs, Corpus
from .keyword_extraction_main import predict
from .canonizer import  process

def extract_keywords(text, model, dictionary, sp, stemmer, args):
    all_docs = [[1, text]]
    df_test = pd.DataFrame(all_docs)
    df_test.columns = ["id", "text"]
    corpus = Corpus(df_test, dictionary, sp, args)
    test_data = batchify_docs(corpus.test, 1)
    model.eval()
    predictions = predict(test_data, model, sp, corpus, args)
    predictions_lemmas = []
    for kw in predictions:
        if len(kw.split()) == 1:
            lemma_kw = stemmer(kw)
        else:
            lemma_kw = kw
        predictions_lemmas.append(lemma_kw)
    root_kws = {}
    for idx, lemma in enumerate(predictions_lemmas):
        if not lemma in root_kws:
            root_kws[lemma] = predictions[idx]
    present = []
    for idx, lemma in enumerate(predictions_lemmas):
        kw = root_kws[lemma] 
        present.append(kw)
    #Canonize
    if len(present) > 0:
        canonnized = process(present[:args['kw_cut']], args['nlp'], args['lem_genders'])      
        return canonnized
    else:
        return present

