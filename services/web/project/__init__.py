import os
import classla
import json


classla.download('sl', logging_level='WARNING')


from flask import (
    Flask,
    jsonify,
    send_from_directory,
    request,
    redirect,
    url_for
)

from flask_restx import Api, Resource, fields, abort, reqparse
import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
from werkzeug.utils import secure_filename
from werkzeug.middleware.proxy_fix import ProxyFix

from . import api_functions
from . import keyword_extraction_main as kw


from lemmagen3 import Lemmatizer
BASEDIR = os.path.dirname(__file__)

lemmatizer = Lemmatizer('sl').lemmatize

lem_genders = {}


# Cannonizer preload
for name, m_path in [('m','-adj-male'),('f','-adj-female'),('n','-adj-neutral'),('base','')]:
    lem_genders[name] = Lemmatizer()
    lem_genders[name].load_model(os.path.join(BASEDIR,f'lemmagen_models/kanon{m_path}.bin'))

print(lem_genders)
nlp = classla.Pipeline(lang='sl', processors='tokenize,pos,lemma', tokenize_pretokenized=True, logging_level='WARNING')

app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
api = Api(app, version='1.0',
          title='API services',
          description='Slovenian keyword extraction REST API')
ns = api.namespace('rest_api', description='REST services API')

args = {
    'max_length': 256,
    'cuda': False,
    'kw_cut': 10,
    'stemmer': lemmatizer,
    'nlp' : nlp,
    'lem_genders' : lem_genders,
    'split_docs': True,   
    'bpe': True, 
}

kw_model_path = "model_SloBIG_folder_slovenian_loss_3.7696915065382153_epoch_9.pt"
kw_dictionary_path = "SloBIG_adaptive_lm_bpe_nopos_rnn_nocrf.ptb"

kw_model = kw.loadModel(os.path.join("project", "trained_classification_models", kw_model_path), args['cuda'])
kw_dictionary = kw.loadDict(os.path.join("project", "dictionaries", kw_dictionary_path)) 
kw_sp = kw.spm.SentencePieceProcessor()
kw_sp.Load(os.path.join("project","bpe", "SloBPE.model"))


# input and output definitions
kw_extractor_input = api.model('KeywordExtractorInput', {
    'text': fields.String(required=True, description='Title + lead + body of the article'),
})

kw_extractor_output = api.model('KeywordExtractorOutput', {
    'keywords': fields.List(fields.String, description='Extracted keywords'),
})


@ns.route('/extract_keywords/')
class KeywordExtractor(Resource):
    @ns.doc('Extracts keywords from news article')
    @ns.expect(kw_extractor_input, validate=True)
    @ns.marshal_with(kw_extractor_output)
    def post(self):
        kw_lem = api_functions.extract_keywords(api.payload['text'], kw_model, kw_dictionary, kw_sp, lemmatizer, args)
        return {"keywords": kw_lem}



@ns.route('/health/')
class Health(Resource):
    @ns.response(200, "successfully fetched health details")
    def get(self):
        return {"status": "running", "message": "Health check successful"}, 200, {}
